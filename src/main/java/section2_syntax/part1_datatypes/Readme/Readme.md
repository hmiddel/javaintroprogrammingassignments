# Java data types

## Learning outcomes
* getting to know Java primitive types
* basic programming

## Assignment details
Simply work your way through the methods and implement them according to the instructions 
stated within the methods' Javadoc or in the method body 

