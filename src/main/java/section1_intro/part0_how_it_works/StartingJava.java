package section1_intro.part0_how_it_works;

/**
 * Creation date: Jun 26, 2017
 *
 * @version 0.01
 * @author Michiel Noback (&copy; 2017)
 */
public class StartingJava {
    public static final String[] GREETINGS = new String[]{
            "Hallo",
            "Moi",
            "Wazzup",
            "Yo!",
            "Hey"
    };

    /**
     * Method simply prints "Hello, World" to the console.
     *
     */
    public void printHelloWorld() {
        System.out.print("Hello, World");
    }

    /**
     * returns the sum of x and y
     * @param x
     * @param y
     * @return theSum
     */
    public int addInts(int x, int y) {
        return 0;
    }

    /**
     * divides x by y and returns the rounded value (rounded to nearest integer)
     * @param x
     * @param y
     * @return dividedAndRounded
     */
    public long divideAndRound(double x, double y) {
        //Math may be a nice class here!
        return 0;
    }

    /**
     * returns the string found at the corresponding position of GREETINGS
     * @param index of the greeting
     * @return greeting
     */
    public String getGreeting(int index) {
        return "";
    }


    /**
     * Returns a Duck object with the given.
     * The Duck class is already present in this same package.
     * @param swimSpeed
     * @param nameOfDuck
     * @return duck
     */
    public Duck createDuck(int swimSpeed, String nameOfDuck) {
        return null;
    }

}
